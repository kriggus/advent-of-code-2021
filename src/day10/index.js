import * as fs from 'fs'

const readNavigationSystem = () =>
  fs
    .readFileSync('./src/day10/input.txt', 'utf-8')
    .split('\n')
    .filter(l => l)
    .map(l => [...l])

const getFirstInvalidChunkChars = (systemCode) =>
  systemCode
    .map(line => findFirstInvalidClosingChunkChar(line))
    .filter(ch => ch)

const findFirstInvalidClosingChunkChar = (line) => {
  const stack = []
  for (const char of line) {
    if (isOpeningChunk(char)) {
      stack.push(char)
    } else if (isValidClosingChar(char, stack)) {
      stack.pop()
    } else {
      return char
    }
  }
  return undefined
}

const isValidClosingChar = (char, stack) =>
  isClosingChunk(char) && isValidChunk(stack[stack.length - 1], char)

const isValidChunk = (opening, closing) =>
  (opening === '(' && closing === ')') ||
  (opening === '[' && closing === ']') ||
  (opening === '{' && closing === '}') ||
  (opening === '<' && closing === '>')

const isOpeningChunk = (char) =>
  ['(', '[', '{', '<'].includes(char)

const isClosingChunk = (char) =>
  [')', ']', '}', '>'].includes(char)

const calcSyntaxErrorScore = (invalidChars) =>
  noOfChars(invalidChars, ')') * 3 +
    noOfChars(invalidChars, ']') * 57 +
    noOfChars(invalidChars, '}') * 1197 +
    noOfChars(invalidChars, '>') * 25137

const noOfChars = (chars, char) =>
  chars.reduce(
    (no, ch) => no + (ch === char ? 1 : 0),
    0
  )

const removeInvalidLines = (systemCode) =>
  systemCode.filter(line =>
    findFirstInvalidClosingChunkChar(line) === undefined
  )

const calcAutoCompleteScore = (incompleteCode) => {
  const completionStrings = incompleteCode.map(line => getCompletionStrings(line))
  const completionStringsScores = completionStrings
    .map(completionString => calcCompletionStringScore(completionString))
    .sort((s1, s2) => s1 - s2)
  const middle = Math.floor(completionStringsScores.length / 2)
  return completionStringsScores[middle]
}

const getCompletionStrings = (line) => {
  const stack = []
  for (const char of line) {
    if (isOpeningChunk(char)) {
      stack.push(char)
    } else if (isClosingChunk(char)) {
      stack.pop()
    }
  }
  return stack
    .reverse()
    .map(char => getCharClosingChunk(char))
}

const getCharClosingChunk = (char) => {
  switch (char) {
    case '(': return ')'
    case '[': return ']'
    case '{': return '}'
    case '<': return '>'
    default: throw Error(`Unrecognized char ${char}`)
  }
}

const calcCompletionStringScore = (completionString) =>
  completionString.reduce(
    (score, char) => score * 5 + calcCompetionCharScore(char),
    0
  )

const calcCompetionCharScore = (char) => {
  switch (char) {
    case ')': return 1
    case ']': return 2
    case '}': return 3
    case '>': return 4
    default: throw Error(`Unrecognized char ${char}`)
  }
}

export const solvePart1 = () => {
  const systemCode = readNavigationSystem()
  const invalidChars = getFirstInvalidChunkChars(systemCode)
  return calcSyntaxErrorScore(invalidChars)
}

export const solvePart2 = () => {
  const systemCode = readNavigationSystem()
  const incompleteCode = removeInvalidLines(systemCode)
  return calcAutoCompleteScore(incompleteCode)
}
