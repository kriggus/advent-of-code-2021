import * as fs from 'fs'

const readTargetArea = () => {
  const text = fs.readFileSync('./src/day17/input.txt', 'utf-8').trim()
  const matches = [...text.matchAll(/([-+]?\d+)..([-+]?\d+)/g)]
  return {
    xMin: parseInt(matches[0][1]),
    xMax: parseInt(matches[0][2]),
    yMin: parseInt(matches[1][1]),
    yMax: parseInt(matches[1][2])
  }
}

const findMinStartVelocityX = ({ xMin }) => {
  let xVel = xMin
  let i = 0
  while (xVel > 0) {
    i++
    xVel -= i
  }
  return i
}

const findMaxPositionY = (target, xStartVelocity) => {
  let yMaxMaxPosition = -1
  let yMaxPosition = -1
  for (let yStartVelocity = 0; yStartVelocity <= Math.abs(target.yMin); yStartVelocity++) {
    yMaxPosition = simulateProbeShot(target, xStartVelocity, yStartVelocity)
    yMaxMaxPosition = Math.max(yMaxMaxPosition, yMaxPosition)
  }
  return yMaxMaxPosition
}

const findAllHitStartVelocities = (target) => {
  const hits = []
  for (let xStartVel = 1; xStartVel <= target.xMax; xStartVel++) {
    for (let yStartVel = target.yMin; yStartVel <= Math.abs(target.yMin); yStartVel++) {
      if (simulateProbeShot(target, xStartVel, yStartVel) !== -1) {
        hits.push({ x: xStartVel, y: yStartVel })
      }
    }
  }
  return hits
}

const simulateProbeShot = (target, xStartVelocity, yStartVelocity) => {
  let xVel = xStartVelocity
  let yVel = yStartVelocity
  let xPos = 0
  let yPos = 0
  let yMaxPos = 0
  while (yPos >= target.yMin) {
    xPos = xPos + xVel
    yPos = yPos + yVel
    yMaxPos = Math.max(yMaxPos, yPos)
    xVel = Math.max(0, xVel - 1)
    yVel = yVel - 1
    if (xPos >= target.xMin && xPos <= target.xMax && yPos >= target.yMin && yPos <= target.yMax) {
      return yMaxPos
    }
  }
  return -1
}

export const solvePart1 = () => {
  const target = readTargetArea()
  const xStartVelocity = findMinStartVelocityX(target)
  return findMaxPositionY(target, xStartVelocity)
}

export const solvePart2 = () => {
  const target = readTargetArea()
  return findAllHitStartVelocities(target).length
}
