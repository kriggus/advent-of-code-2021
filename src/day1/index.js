import * as fs from 'fs'

const readDepthList = () =>
  fs.readFileSync('./src/day1/input.txt', 'utf8')
    .split('\n')
    .map(line => parseInt(line))
    .filter(depth => depth !== undefined && isFinite(depth))

const calcWindowDepth = (depths, i) =>
  depths[i] + depths[i + 1] + depths[i + 2]

export const solvePart1 = () => {
  const depths = readDepthList()
  let lastDepth
  let noOfDepthIncreases = 0
  depths.forEach(depth => {
    if (lastDepth && depth > lastDepth) {
      noOfDepthIncreases++
    }
    lastDepth = depth
  })
  return noOfDepthIncreases
}

export const solvePart2 = () => {
  const depths = readDepthList()
  let lastWindowDepth
  let noOfWindowDepthIncreases = 0
  for (let i = 0; i < depths.length; i++) {
    const windowDepth = calcWindowDepth(depths, i)
    if (isFinite(lastWindowDepth) && isFinite(windowDepth) && windowDepth > lastWindowDepth) {
      noOfWindowDepthIncreases++
    }
    lastWindowDepth = windowDepth
  }
  return noOfWindowDepthIncreases
}
