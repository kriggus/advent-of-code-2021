import * as fs from 'fs'

const readDiagnosticReport = () =>
  fs.readFileSync('./src/day3/input.txt', 'utf8')
    .split('\n')
    .filter(l => l)

const collectBitCountings = (diagnostics) =>
  diagnostics.reduce(
    (acc, binary) => {
      for (let i = 0; i < binary.length; i++) {
        const bitCounting = acc[i] ?? { noOfZero: 0, noOfOne: 0 }
        if (binary[i] === '0') {
          bitCounting.noOfZero++
        } else if (binary[i] === '1') {
          bitCounting.noOfOne++
        }
        acc[i] = bitCounting
      }
      return acc
    }, []
  )

const calcGammaRate = (bitCountings) => {
  const binary = bitCountings.map(bc => bc.noOfOne > bc.noOfZero ? '1' : '0').join('')
  return parseInt(binary, 2)
}

const calcEpsilonRate = (bitCountings) => {
  const binary = bitCountings.map(bc => bc.noOfOne < bc.noOfZero ? '1' : '0').join('')
  return parseInt(binary, 2)
}

export const solvePart1 = () => {
  const diagnostics = readDiagnosticReport()
  const bitCountings = collectBitCountings(diagnostics)
  const gammaRate = calcGammaRate(bitCountings)
  const epsilonRate = calcEpsilonRate(bitCountings)
  return gammaRate * epsilonRate
}

const findRating = (diagnostics, selectBitCreteria) => {
  let ratings = [...diagnostics]
  let i = 0
  while (ratings.length > 1) {
    const bitCountings = collectBitCountings(ratings)
    const bitCreteria = selectBitCreteria(bitCountings[i])
    ratings = ratings.filter(binary => binary[i] === bitCreteria)
    i++
  }
  return parseInt(ratings[0], 2)
}

export const solvePart2 = () => {
  const diagnostics = readDiagnosticReport()
  const oxygenGeneratorRating = findRating(diagnostics, bc => bc.noOfOne >= bc.noOfZero ? '1' : '0')
  const co2ScrubberRating = findRating(diagnostics, bc => bc.noOfOne < bc.noOfZero ? '1' : '0')
  return oxygenGeneratorRating * co2ScrubberRating
}
