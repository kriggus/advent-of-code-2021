import * as fs from 'fs'

const readLines = () =>
  fs.readFileSync('./src/day5/input.txt', 'utf-8')
    .split('\n')
    .filter(l => l)
    .map(l => l.split(' -> ').flatMap(ps => ps.split(',')))
    .map(cs => ({
      p1: {
        x: parseInt(cs[0]),
        y: parseInt(cs[1])
      },
      p2: {
        x: parseInt(cs[2]),
        y: parseInt(cs[3])
      }
    }))

const filterPerpendicularLines = (lines) =>
  lines.filter(({ p1, p2 }) => p1.x === p2.x || p1.y === p2.y)

const sizeOfMap = (lines) => {
  let xMax = 0
  let yMax = 0
  lines.forEach(({ p1, p2 }) => {
    xMax = Math.max(xMax, Math.max(p1.x, p2.x))
    yMax = Math.max(yMax, Math.max(p1.y, p2.y))
  })
  return {
    xSize: xMax + 1,
    ySize: yMax + 1
  }
}

const makeMap = (lines) => {
  const { xSize, ySize } = sizeOfMap(lines)
  return new Array(xSize).fill([]).map(_ => new Array(ySize).fill(0))
}

const fillMap = (map, lines) => {
  lines.forEach(({ p1, p2 }) => {
    const xDirection = Math.sign(p2.x - p1.x)
    const yDirection = Math.sign(p2.y - p1.y)
    const xLength = Math.abs(p2.x - p1.x)
    const yLength = Math.abs(p2.y - p1.y)
    const length = Math.max(xLength, yLength)
    for (let i = 0; i <= length; i++) {
      const x = p1.x + xDirection * i
      const y = p1.y + yDirection * i
      map[x][y]++
    }
  })
}

const noOfOverlapsAbove = (map, above) =>
  map.flat().filter(noOfOverlap => noOfOverlap > above).length

export const solvePart1 = () => {
  const lines = filterPerpendicularLines(readLines())
  const map = makeMap(lines)
  fillMap(map, lines)
  return noOfOverlapsAbove(map, 1)
}

export const solvePart2 = () => {
  const lines = readLines()
  const map = makeMap(lines)
  fillMap(map, lines)
  return noOfOverlapsAbove(map, 1)
}
