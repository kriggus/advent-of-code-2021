import * as fs from 'fs'

const readOctopusEnergyLevels = () => {
  const energyLevelsYX = fs
    .readFileSync('./src/day11/input.txt', 'utf-8')
    .split('\n')
    .filter(l => l)
    .map(row => [...row].map(s => parseInt(s)))
  const energyLevels = []
  for (let x = 0; x < ySize(energyLevelsYX); x++) {
    for (let y = 0; y < xSize(energyLevelsYX); y++) {
      energyLevels[x] = energyLevels[x]
        ? [...energyLevels[x], energyLevelsYX[y][x]]
        : [energyLevelsYX[y][x]]
    }
  }
  return energyLevels
}

const xSize = (energyMap) =>
  energyMap.length

const ySize = (energyMap) =>
  energyMap[0]?.length ?? 0

const simulateOctopusesNumberOfFlashes = (energyMap, noOfGenerations) => {
  let totalFlashes = 0
  for (let gen = 1; gen <= noOfGenerations; gen++) {
    increaseAllEnergyLevels(energyMap, noOfGenerations)
    totalFlashes += simulateFlashes(energyMap)
  }
  return totalFlashes
}

const simulateOctopusesUntilFirstSyncFlash = (energyMap, noOfGenerations) => {
  for (let gen = 1; gen <= noOfGenerations; gen++) {
    increaseAllEnergyLevels(energyMap, noOfGenerations)
    const noOfFlashes = simulateFlashes(energyMap)
    if (noOfFlashes === xSize(energyMap) * ySize(energyMap)) {
      return gen
    }
  }
  return -1
}

const simulateFlashes = (energyMap) => {
  const flashedPositions = []
  let noOfFlashes = 0
  let didFlash
  do {
    const positionsToFlash = getPositionsToFlash(energyMap, flashedPositions)
    increaseEnergyLevelsAdjacentTo(energyMap, positionsToFlash)
    flashedPositions.push(...positionsToFlash)
    noOfFlashes += positionsToFlash.length
    didFlash = positionsToFlash.length > 0
  } while (didFlash)
  resetEnergyLevelsAt(energyMap, flashedPositions)
  return noOfFlashes
}

const getPositionsToFlash = (energyMap, positionsFlashed) => {
  const toFlash = []
  for (let x = 0; x < xSize(energyMap); x++) {
    for (let y = 0; y < ySize(energyMap); y++) {
      if (energyMap[x][y] > 9 && !positionsFlashed.some(p => x === p.x && y === p.y)) {
        toFlash.push({ x, y })
      }
    }
  }
  return toFlash
}

const increaseAllEnergyLevels = (energyMap) => {
  for (let x = 0; x < xSize(energyMap); x++) {
    for (let y = 0; y < ySize(energyMap); y++) {
      energyMap[x][y]++
    }
  }
}

const increaseEnergyLevelsAdjacentTo = (energyMap, positions) => {
  for (const position of positions) {
    const adjecentPositions = getAdjacentPositions(energyMap, position)
    for (const { x, y } of adjecentPositions) {
      energyMap[x][y]++
    }
  }
}

const resetEnergyLevelsAt = (energyMap, positions) => {
  for (const { x, y } of positions) {
    energyMap[x][y] = 0
  }
}

const getAdjacentPositions = (energyMap, origin) => {
  const adjacent = []
  for (let x = origin.x - 1; x <= origin.x + 1; x++) {
    for (let y = origin.y - 1; y <= origin.y + 1; y++) {
      const isNotOrigin = x !== origin.x || y !== origin.y
      const isOnMap = x >= 0 && x < xSize(energyMap) && y >= 0 && y < ySize(energyMap)
      if (isNotOrigin && isOnMap) {
        adjacent.push({ x, y })
      }
    }
  }
  return adjacent
}

export const solvePart1 = () => {
  const energyMap = readOctopusEnergyLevels()
  return simulateOctopusesNumberOfFlashes(energyMap, 100)
}

export const solvePart2 = () => {
  const energyMap = readOctopusEnergyLevels()
  return simulateOctopusesUntilFirstSyncFlash(energyMap, 1000)
}
