import * as fs from 'fs'

const readTransmission = () =>
  fs
    .readFileSync('./src/day16/input.txt', 'utf-8')
    .split('\n')[0]
    .split('')

const toBinary = (hex) =>
  hex.flatMap(char => {
    switch (char) {
      case '0': return [0, 0, 0, 0]
      case '1': return [0, 0, 0, 1]
      case '2': return [0, 0, 1, 0]
      case '3': return [0, 0, 1, 1]
      case '4': return [0, 1, 0, 0]
      case '5': return [0, 1, 0, 1]
      case '6': return [0, 1, 1, 0]
      case '7': return [0, 1, 1, 1]
      case '8': return [1, 0, 0, 0]
      case '9': return [1, 0, 0, 1]
      case 'A': return [1, 0, 1, 0]
      case 'B': return [1, 0, 1, 1]
      case 'C': return [1, 1, 0, 0]
      case 'D': return [1, 1, 0, 1]
      case 'E': return [1, 1, 1, 0]
      case 'F': return [1, 1, 1, 1]
      default: throw Error(`Unsupported char ${char}`)
    }
  })

const toDecimal = (bits) => {
  let dec = 0
  bits.forEach((bit, i) => {
    const pow = bits.length - 1 - i
    dec += bit * Math.pow(2, pow)
  })
  return dec
}

const parseTransmission = (bits, maxNoOfPackets = undefined) => {
  const expressions = []
  let expression = { }
  let version
  let type

  let literal
  let literalOffset
  let literalState
  let literalBit

  let subExpressionLengthBits
  let subExpressionLength
  let subExpressionPackets
  let subExpressionResult

  let packet = 'version'
  let packetBits

  let noOfPackets = 0
  let index = 0
  while (index < bits.length) {
    if (maxNoOfPackets !== undefined && noOfPackets >= maxNoOfPackets) {
      return { expressions, noOfParsedBits: index }
    }
    switch (packet) {
      case 'version':
        if (bits
          .slice(index, bits.length)
          .reduce((isAllZero, bit) => isAllZero && bit === 0, true)
        ) {
          index = bits.length
        } else {
          packetBits = bits.slice(index, index + 3)
          version = toDecimal(packetBits)
          expression = { version }
          packet = 'type'
          index += 3
        }
        break
      case 'type':
        packetBits = bits.slice(index, index + 3)
        type = toDecimal(packetBits)
        expression = { ...expression, type }
        packet = type === 4 ? 'literal' : 'operator'
        index += 3
        break
      case 'literal':
        packetBits = []
        literalState = 'started'
        literalOffset = 0
        while (literalState !== 'ended') {
          literalBit = bits[index + literalOffset]
          if (literalOffset % 5 === 0) {
            literalState = literalBit === 0 ? 'ending' : literalState
          } else if (literalOffset % 5 === 4) {
            packetBits.push(literalBit)
            literalState = literalState === 'ending' ? 'ended' : literalState
          } else {
            packetBits.push(literalBit)
          }
          literalOffset++
        }
        literal = toDecimal(packetBits)
        expression = { ...expression, literal }
        expressions.push(expression)
        packet = 'version'
        noOfPackets++
        index += literalOffset
        break
      case 'operator':
        if (bits[index] === 0) {
          subExpressionLengthBits = bits.slice(index + 1, index + 16)
          subExpressionLength = toDecimal(subExpressionLengthBits)
          packetBits = bits.slice(index + 16, index + 16 + subExpressionLength)
          subExpressionResult = parseTransmission(packetBits)
          expression = { ...expression, expressions: subExpressionResult.expressions }
          expressions.push(expression)
          packet = 'version'
          noOfPackets++
          index += 16 + subExpressionLength
        } else {
          subExpressionLengthBits = bits.slice(index + 1, index + 12)
          subExpressionPackets = toDecimal(subExpressionLengthBits)
          packetBits = bits.slice(index + 12)
          subExpressionResult = parseTransmission(packetBits, subExpressionPackets)
          expression = { ...expression, expressions: subExpressionResult.expressions }
          expressions.push(expression)
          packet = 'version'
          noOfPackets++
          index += 12 + subExpressionResult.noOfParsedBits
        }
        break
      default:
        throw Error(`Unsupported state ${packet}`)
    }
  }

  return { expressions, noOfParsedBits: index }
}

const versionSum = (expressions) =>
  expressions.reduce(
    (sum, expr) => sum + expr.version + (expr.expressions ? versionSum(expr.expressions) : 0),
    0
  )

const solveExpression = (expr) => {
  let solvedValue
  switch (expr.type) {
    case 0:
      solvedValue = solveSumExpressions(expr.expressions)
      break
    case 1:
      solvedValue = solveProductExpressions(expr.expressions)
      break
    case 2:
      solvedValue = solveMinExpression(expr.expressions)
      break
    case 3:
      solvedValue = solveMaxExpression(expr.expressions)
      break
    case 4:
      solvedValue = expr.literal
      break
    case 5:
      solvedValue = solveExpression(expr.expressions[0]) > solveExpression(expr.expressions[1]) ? 1 : 0
      break
    case 6:
      solvedValue = solveExpression(expr.expressions[0]) < solveExpression(expr.expressions[1]) ? 1 : 0
      break
    case 7:
      solvedValue = solveExpression(expr.expressions[0]) === solveExpression(expr.expressions[1]) ? 1 : 0
      break
    default:
      throw new Error(`Unsupported expression type ${expr.type}`)
  }
  return solvedValue
}

const solveSumExpressions = (expressions) =>
  expressions.reduce((sum, expr) => sum + solveExpression(expr), 0)

const solveProductExpressions = (expressions) =>
  expressions.reduce((mul, expr) => mul * solveExpression(expr), 1)

const solveMinExpression = (expressions) =>
  expressions.reduce((min, expr) => Math.min(min, solveExpression(expr)), Number.MAX_SAFE_INTEGER)

const solveMaxExpression = (expressions) =>
  expressions.reduce((max, expr) => Math.max(max, solveExpression(expr)), Number.MIN_SAFE_INTEGER)

export const solvePart1 = () => {
  const hex = readTransmission()
  const bits = toBinary(hex)
  const { expressions } = parseTransmission(bits)
  return versionSum(expressions)
}

export const solvePart2 = () => {
  const hex = readTransmission()
  const bits = toBinary(hex)
  const { expressions } = parseTransmission(bits)
  return solveExpression(expressions[0])
}
