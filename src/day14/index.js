import * as fs from 'fs'

const readPolymerAndInsertions = () => {
  const [polymer, insertsStr] = fs
    .readFileSync('./src/day14/input.txt', 'utf-8')
    .split('\n\n')
  const inserts = insertsStr
    .split('\n')
    .filter(l => l)
    .map(l => {
      const [pattern, char] = l.split(' -> ')
      return { pattern, char }
    })
  return { polymer, inserts }
}

const startCounts = (polymer) =>
  polymer
    .split('')
    .reduce(
      (counts, char, i, chars) => {
        counts.char[char] = counts.char[char] ? counts.char[char] + 1 : 1
        if (i < chars.length - 1) {
          const pattern = char + chars[i + 1]
          counts.pattern[pattern] = counts.pattern[pattern] ? counts.pattern[pattern] + 1 : 1
        }
        return counts
      },
      { char: { }, pattern: { } }
    )

const inflateCharCounts = (charCounts, patternCounts, inserts, noOfIterations, iteration = 0) => {
  if (iteration === noOfIterations) {
    return charCounts
  }
  const newPatternCounts = {}
  for (const insert of inserts) {
    for (const pattern of Object.keys(patternCounts)) {
      const count = patternCounts[pattern]
      if (insert.pattern === pattern && count > 0) {
        const newPattern1 = pattern[0] + insert.char
        const newPattern2 = insert.char + pattern[1]
        newPatternCounts[newPattern1] = newPatternCounts[newPattern1] ? newPatternCounts[newPattern1] + count : count
        newPatternCounts[newPattern2] = newPatternCounts[newPattern2] ? newPatternCounts[newPattern2] + count : count
        charCounts[insert.char] = charCounts[insert.char] ? charCounts[insert.char] + count : count
      }
    }
  }
  return inflateCharCounts(charCounts, newPatternCounts, inserts, noOfIterations, iteration + 1)
}

const checksum = (charCounts) => {
  const sorted = Object.values(charCounts).filter(c => c > 0).sort((a, b) => a - b)
  return sorted[sorted.length - 1] - sorted[0]
}

export const solvePart1 = () => {
  const { polymer, inserts } = readPolymerAndInsertions()
  const counts = startCounts(polymer)
  const inflatedCharCounts = inflateCharCounts(counts.char, counts.pattern, inserts, 10)
  return checksum(inflatedCharCounts)
}

export const solvePart2 = () => {
  const { polymer, inserts } = readPolymerAndInsertions()
  const counts = startCounts(polymer)
  const inflatedCharCounts = inflateCharCounts(counts.char, counts.pattern, inserts, 40)
  return checksum(inflatedCharCounts)
}
