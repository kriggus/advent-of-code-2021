import * as fs from 'fs'

const readCommands = () =>
  fs.readFileSync('./src/day2/input.txt', 'utf8')
    .split('\n')
    .filter(line => line)
    .map(line => {
      const parts = line.split(' ')
      return {
        type: parts[0],
        amount: parseInt(parts[1])
      }
    })

export const solvePart1 = () => {
  let hPos = 0
  let depth = 0
  for (const command of readCommands()) {
    switch (command.type) {
      case 'forward':
        hPos = Math.max(0, hPos + command.amount)
        break
      case 'up':
        depth = Math.max(0, depth - command.amount)
        break
      case 'down':
        depth = Math.max(0, depth + command.amount)
        break
      default:
        throw Error(`Unknown command ${command.type}`)
    }
  }
  return hPos * depth
}

export const solvePart2 = () => {
  let hPos = 0
  let depth = 0
  let aim = 0
  for (const command of readCommands()) {
    switch (command.type) {
      case 'forward':
        hPos = Math.max(0, hPos + command.amount)
        depth = Math.max(0, depth + aim * command.amount)
        break
      case 'up':
        aim = Math.max(0, aim - command.amount)
        break
      case 'down':
        aim = Math.max(0, aim + command.amount)
        break
      default:
        throw Error(`Unknown command ${command.type}`)
    }
  }
  return hPos * depth
}
