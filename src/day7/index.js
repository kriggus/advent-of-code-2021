import * as fs from 'fs'

const readCrabPositions = () =>
  fs
    .readFileSync('./src/day7/input.txt', 'utf-8')
    .split(',')
    .map((s) => parseInt(s))

const calcPositionSpan = (positions) => {
  let min = Number.MAX_SAFE_INTEGER
  let max = 0
  positions.forEach(position => {
    min = Math.min(min, position)
    max = Math.max(max, position)
  })
  return { minPos: min, maxPos: max }
}

const calcFuelSpent = (positions, targetPos) =>
  positions.reduce((acc, pos) => acc + Math.abs(pos - targetPos), 0)

const calcFuelSpentWithGrowingCost = (positions, targetPos) => {
  const costByNoOfSteps = [0, 1]
  return positions.reduce(
    (acc, pos) => {
      const noOfSteps = Math.abs(pos - targetPos)
      const cost = costByNoOfSteps[noOfSteps] ?? calcFuelCost(noOfSteps)
      costByNoOfSteps[noOfSteps] = cost
      return acc + cost
    },
    0
  )
}

const calcFuelCost = (noOfSteps) => {
  let cost = 0
  for (let no = 0; no <= noOfSteps; no++) {
    cost += no
  }
  return cost
}

export const solvePart1 = () => {
  const positions = readCrabPositions()
  const { minPos, maxPos } = calcPositionSpan(positions)
  let fuelSpent = Number.MAX_SAFE_INTEGER
  for (let pos = minPos; pos <= maxPos; pos++) {
    fuelSpent = Math.min(fuelSpent, calcFuelSpent(positions, pos))
  }
  return fuelSpent
}

export const solvePart2 = () => {
  const positions = readCrabPositions()
  const { minPos, maxPos } = calcPositionSpan(positions)
  let fuelSpent = Number.MAX_SAFE_INTEGER
  for (let pos = minPos; pos <= maxPos; pos++) {
    const spentFromPos = calcFuelSpentWithGrowingCost(positions, pos)
    fuelSpent = Math.min(fuelSpent, spentFromPos)
  }
  return fuelSpent
}
