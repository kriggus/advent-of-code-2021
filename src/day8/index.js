import * as fs from 'fs'

const readDisplaySignals = () =>
  fs
    .readFileSync('./src/day8/input.txt', 'utf-8')
    .split('\n')
    .filter(l => l)
    .map(l => {
      const [signalDigitPatternsStr, outputDigitPatternsStr] = l.split(' | ')
      return {
        signalDigitPatterns: signalDigitPatternsStr.split(' '),
        outputDigitPatterns: outputDigitPatternsStr.split(' ')
      }
    })

const countNoOfDigit1478 = (displaySignals) =>
  displaySignals.reduce(
    (no, { outputDigitPatterns }) => {
      const matches = outputDigitPatterns.filter(pattern => [2, 3, 4, 7].includes(pattern.length))
      return no + matches.length
    },
    0
  )

const deduceDigitPatterns = (patterns) => {
  const one = patterns.find(p => p.length === 2)
  const four = patterns.find(p => p.length === 4)
  const seven = patterns.find(p => p.length === 3)
  const eight = patterns.find(p => p.length === 7)

  const nine = patterns.find(p => p.length === 6 && [...four].every(s => p.includes(s)))
  const zero = patterns.find(p => p.length === 6 && p !== nine && [...one].every(s => p.includes(s)))
  const six = patterns.find(p => p.length === 6 && p !== nine && p !== zero)

  const three = patterns.find(p => p.length === 5 && [...one].every(s => p.includes(s)))
  const five = patterns.find(p => p.length === 5 && p !== three && [...p].every(s => nine.includes(s)))
  const two = patterns.find(p => p.length === 5 && p !== three && p !== five)

  return [zero, one, two, three, four, five, six, seven, eight, nine]
}

const patternsToDigets = (patterns, patternsWithIndexAsDiget) =>
  patterns.map(pattern =>
    patternsWithIndexAsDiget.findIndex(otherPattern => isSamePattern(pattern, otherPattern))
  )

const isSamePattern = (p1, p2) =>
  p1.length === p2.length && [...p1].every(s1 => p2.includes(s1))

const valueOfDigits = (digets) =>
  digets.reduce(
    (value, diget, i) => {
      const pow = digets.length - i - 1
      return value + diget * Math.pow(10, pow)
    },
    0
  )

export const solvePart1 = () => {
  const displaySignals = readDisplaySignals()
  return countNoOfDigit1478(displaySignals)
}

export const solvePart2 = () => {
  const displaySignals = readDisplaySignals()
  return displaySignals.reduce((sum, { signalDigitPatterns, outputDigitPatterns }) => {
    const digitPatternsWithIndexAsDiget = deduceDigitPatterns(signalDigitPatterns)
    const outputDigets = patternsToDigets(outputDigitPatterns, digitPatternsWithIndexAsDiget)
    const outputValue = valueOfDigits(outputDigets)
    return sum + outputValue
  }, 0)
}
