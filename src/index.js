import { solvePuzzle } from './puzzleSolver.js'

try {
  const day = parseInt(process.argv[2])
  const part = parseInt(process.argv[3])
  const solution = solvePuzzle(day, part)
  console.log(solution)
} catch (err) {
  console.error(err)
}
