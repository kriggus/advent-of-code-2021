import * as fs from 'fs'

const readTransparentPaper = () => {
  const [coords, instructions] = fs
    .readFileSync('./src/day13/input.txt', 'utf-8')
    .split('\n\n')
  return {
    ...readDotsAndSize(coords),
    instructions: readInstructions(instructions)
  }
}

const readDotsAndSize = (dotsStr) => {
  let xMax = 0
  let yMax = 0
  const dots = []
  dotsStr
    .split('\n')
    .forEach(line => {
      const [x, y] = line.split(',').map(coord => parseInt(coord))
      xMax = Math.max(xMax, x)
      yMax = Math.max(yMax, y)
      dots.push({ x, y })
    })
  return {
    dots,
    size: { x: xMax + 1, y: yMax + 1 }
  }
}

const readInstructions = (instructionsStr) => {
  return instructionsStr
    .split('\n')
    .filter(l => l)
    .map(l => {
      const [txt, valueStr] = l.split('=')
      return { dimension: txt[txt.length - 1], at: parseInt(valueStr) }
    })
}

const fold = (paper, maxFoldes) => {
  let foldedPaper = paper
  paper.instructions.forEach((instruction, index) => {
    if (index < maxFoldes) {
      foldedPaper = instruction.dimension === 'x'
        ? foldX(foldedPaper, instruction.at)
        : foldY(foldedPaper, instruction.at)
    }
  })
  return foldedPaper
}

const foldX = (paper, at) => {
  const newDots = paper.dots.filter(dot => dot.x < at)
  for (let x = 0; x < at; x++) {
    for (let y = 0; y < paper.size.y; y++) {
      const xFolded = Math.min(2 * at, Math.max(2 * at, paper.size.x - 1)) - x
      if (hasDot(paper, xFolded, y) && !hasDot(paper, x, y)) {
        newDots.push({ x, y })
      }
    }
  }
  return {
    ...paper,
    dots: newDots,
    size: {
      ...paper.size,
      x: at
    }
  }
}

const foldY = (paper, at) => {
  const newDots = paper.dots.filter(dot => dot.y < at)
  for (let x = 0; x < paper.size.x; x++) {
    for (let y = 0; y < at; y++) {
      const yFolded = Math.min(2 * at, Math.max(2 * at, paper.size.y - 1)) - y
      if (hasDot(paper, x, yFolded) && !hasDot(paper, x, y)) {
        newDots.push({ x, y })
      }
    }
  }
  return {
    ...paper,
    dots: newDots,
    size: {
      ...paper.size,
      y: at
    }
  }
}

const hasDot = (paper, x, y) =>
  paper.dots.some(dot => dot.x === x && dot.y === y)

const printPaper = (paper) => {
  let str = ''
  for (let y = 0; y < paper.size.y; y++) {
    for (let x = 0; x < paper.size.x; x++) {
      str += hasDot(paper, x, y) ? '#' : '.'
    }
    str += '\n'
  }
  console.log(str)
}

export const solvePart1 = () => {
  const paper = readTransparentPaper()
  const foldedPaper = fold(paper, 1)
  return foldedPaper.dots.length
}

export const solvePart2 = () => {
  const paper = readTransparentPaper()
  const foldedPaper = fold(paper, 99)
  printPaper(foldedPaper)
  return 1
}
