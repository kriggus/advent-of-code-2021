import * as Day1 from './day1/index.js'
import * as Day2 from './day2/index.js'
import * as Day3 from './day3/index.js'
import * as Day4 from './day4/index.js'
import * as Day5 from './day5/index.js'
import * as Day6 from './day6/index.js'
import * as Day7 from './day7/index.js'
import * as Day8 from './day8/index.js'
import * as Day9 from './day9/index.js'
import * as Day10 from './day10/index.js'
import * as Day11 from './day11/index.js'
import * as Day12 from './day12/index.js'
import * as Day13 from './day13/index.js'
import * as Day14 from './day14/index.js'
import * as Day15 from './day15/index.js'
import * as Day16 from './day16/index.js'
import * as Day17 from './day17/index.js'

export const solvePuzzle = (day, part) => {
  switch (`${day}.${part}`) {
    case '1.1':
      return Day1.solvePart1()
    case '1.2':
      return Day1.solvePart2()
    case '2.1':
      return Day2.solvePart1()
    case '2.2':
      return Day2.solvePart2()
    case '3.1':
      return Day3.solvePart1()
    case '3.2':
      return Day3.solvePart2()
    case '4.1':
      return Day4.solvePart1()
    case '4.2':
      return Day4.solvePart2()
    case '5.1':
      return Day5.solvePart1()
    case '5.2':
      return Day5.solvePart2()
    case '6.1':
      return Day6.solvePart1()
    case '6.2':
      return Day6.solvePart2()
    case '7.1':
      return Day7.solvePart1()
    case '7.2':
      return Day7.solvePart2()
    case '8.1':
      return Day8.solvePart1()
    case '8.2':
      return Day8.solvePart2()
    case '9.1':
      return Day9.solvePart1()
    case '9.2':
      return Day9.solvePart2()
    case '10.1':
      return Day10.solvePart1()
    case '10.2':
      return Day10.solvePart2()
    case '11.1':
      return Day11.solvePart1()
    case '11.2':
      return Day11.solvePart2()
    case '12.1':
      return Day12.solvePart1()
    case '12.2':
      return Day12.solvePart2()
    case '13.1':
      return Day13.solvePart1()
    case '13.2':
      return Day13.solvePart2()
    case '14.1':
      return Day14.solvePart1()
    case '14.2':
      return Day14.solvePart2()
    case '15.1':
      return Day15.solvePart1()
    case '15.2':
      return Day15.solvePart2()
    case '16.1':
      return Day16.solvePart1()
    case '16.2':
      return Day16.solvePart2()
    case '17.1':
      return Day17.solvePart1()
    case '17.2':
      return Day17.solvePart2()
    default:
      throw new Error(`Solution for day '${day}' part '${part}' is not implemented`)
  }
}
