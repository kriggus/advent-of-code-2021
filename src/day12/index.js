import * as fs from 'fs'

const readCaveSystem = () => {
  const nodes = { }
  fs
    .readFileSync('./src/day12/input.txt', 'utf-8')
    .split('\n')
    .filter(l => l)
    .map(l => l.split('-'))
    .forEach(edge => {
      const node1 = edge[0]
      const node2 = edge[1]
      nodes[node1] = nodes[node1] ? [node2, ...nodes[node1]] : [node2]
      nodes[node2] = nodes[node2] ? [node1, ...nodes[node2]] : [node1]
    })
  return nodes
}

const calcPaths = (nodes, calcSubPaths) => {
  const paths = []
  nodes.start.forEach(node => {
    const path = ['start', node]
    paths.push(...calcSubPaths(nodes, path))
  })
  return paths
}

const calcSubPathsVisitingSmallOnce = (nodes, path) => {
  const current = path[path.length - 1]
  return isNodeEnd(current)
    ? [path]
    : nodes[current]
      .flatMap(next =>
        isNodeLarge(next) || !isNodeInPath(path, next)
          ? calcSubPathsVisitingSmallOnce(nodes, [...path, next])
          : [null]
      )
      .filter(p => p !== null)
}

const calcSubPathsVisitingSmallTwiceOnce = (nodes, path) => {
  const current = path[path.length - 1]
  return isNodeEnd(current)
    ? [path]
    : nodes[current]
      .flatMap(next =>
        isNodeLarge(next) || !isNodeInPath(path, next) || (!isNodeStart(next) && !hasSmallTwice(path))
          ? calcSubPathsVisitingSmallTwiceOnce(nodes, [...path, next])
          : [null]
      )
      .filter(p => p !== null)
}

const hasSmallTwice = (path) =>
  path.some((n1, i1) =>
    !isNodeLarge(n1) && path.some((n2, i2) =>
      n1 === n2 && i1 !== i2
    )
  )

const isNodeStart = (node) =>
  node === 'start'

const isNodeEnd = (node) =>
  node === 'end'

const isNodeLarge = (node) => {
  const allCaps = /^[A-Z]*$/
  return node.match(allCaps) !== null
}

const isNodeInPath = (path, node) =>
  path.includes(node)

export const solvePart1 = () => {
  const nodes = readCaveSystem()
  const paths = calcPaths(nodes, calcSubPathsVisitingSmallOnce)
  return paths.length
}

export const solvePart2 = () => {
  const nodes = readCaveSystem()
  const paths = calcPaths(nodes, calcSubPathsVisitingSmallTwiceOnce)
  return paths.length
}
