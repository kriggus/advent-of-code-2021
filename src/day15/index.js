import * as fs from 'fs'

const readRisks = () => {
  const risksYX = fs
    .readFileSync('./src/day15/input.txt', 'utf-8')
    .split('\n')
    .filter(l => l)
    .map(l => l.split('').map(s => parseInt(s)))
  return flipXY(risksYX)
}

const flipXY = (coords) =>
  coords.map((_, dim1) =>
    new Array(coords.length).fill(0).map((_, dim2) =>
      coords[dim2][dim1]
    )
  )

const inflateRisks = (risks, n) => {
  const xSize = xMax(risks) + 1
  const ySize = yMax(risks) + 1
  const inflated = []
  for (let x = 0; x < xSize * n; x++) {
    for (let y = 0; y < ySize * n; y++) {
      const xOrigin = x % xSize
      const yOrigin = y % ySize
      const xOffset = Math.floor(x / xSize)
      const yOffset = Math.floor(y / ySize)
      inflated[x] = inflated[x] ?? []
      inflated[x][y] = ((risks[xOrigin][yOrigin] + xOffset + yOffset - 1) % 9) + 1
    }
  }
  return inflated
}

const calcCostOfPath = (risks) => {
  const unvisited = setupPriorityQueue(risks)
  const visited = [[true]]
  const costs = [[0]]
  while (true) {
    const current = take(unvisited)

    const neighbors = findUnvisitedNeighbors(risks, visited, current)
    updateCostsOfNeighbors(risks, unvisited, costs, neighbors, current)
    setVisited(visited, current.x, current.y)
    if (isDestination(risks, current.x, current.y)) {
      return getCost(costs, current.x, current.y)
    }
  }
}

const findUnvisitedNeighbors = (risks, visited, node) => {
  const { x, y } = node
  const neighbors = [{ x: x + 1, y }, { x: x - 1, y }, { x, y: y + 1 }, { x, y: y - 1 }]
  return neighbors.filter(({ x, y }) =>
    isInBounds(risks, x, y) && !isVisited(visited, x, y)
  )
}

const updateCostsOfNeighbors = (risks, unvisited, costs, neighbors, current) => {
  neighbors.forEach(({ x, y }) => {
    const oldCost = getCost(costs, x, y)
    const newCost = getCost(costs, current.x, current.y) + risks[x][y]
    const minCost = Math.min(oldCost, newCost)
    setCost(costs, minCost, x, y)
    decPriority(unvisited, minCost, x, y)
  })
}

const setupPriorityQueue = (risks) =>
  risks.flatMap((column, x) =>
    column.map((_, y) =>
      ({ x, y, prio: x === 0 && y === 0 ? 0 : Number.MAX_SAFE_INTEGER })
    )
  )

const take = (queue) => {
  // remove root and put last as new root
  const root = queue[0]
  queue[0] = queue.splice(queue.length - 1, 1)[0]

  // hipify down
  let index = 0
  let [parentPrio, leftChildPrio, rightChildPrio] = [queue[0].prio, queue[1]?.prio ?? Number.POSITIVE_INFINITY, queue[2]?.prio ?? Number.POSITIVE_INFINITY]
  while (leftChildPrio < parentPrio || rightChildPrio < parentPrio) {
    const childIndex = leftChildPrio < rightChildPrio ? leftChildIndexOf(index) : rightChildIndexOf(index)
    swap(queue, index, childIndex)
    index = childIndex
    parentPrio = queue[index].prio
    leftChildPrio = queue[leftChildIndexOf(index)]?.prio ?? Number.POSITIVE_INFINITY
    rightChildPrio = queue[rightChildIndexOf(index)]?.prio ?? Number.POSITIVE_INFINITY
  }
  return root
}

const decPriority = (queue, prio, x, y) => {
  // update prio
  let index = indexOf(queue, x, y)
  queue[index].prio = prio

  // hipify up
  let parentIndex = parentIndexOf(index)
  let [parentPrio, childPrio] = [queue[parentIndex]?.prio ?? Number.NEGATIVE_INFINITY, queue[index].prio]
  while (parentIndex >= 0 && childPrio < parentPrio) {
    swap(queue, index, parentIndex)
    index = parentIndex
    parentIndex = parentIndexOf(index)
    parentPrio = queue[parentIndex]?.prio ?? Number.NEGATIVE_INFINITY
    childPrio = queue[index].prio
  }
}

const swap = (queue, i1, i2) => {
  const tmp = queue[i1]
  queue[i1] = queue[i2]
  queue[i2] = tmp
}

const indexOf = (queue, x, y) => {
  for (let i = 0; i < queue.length; i++) {
    if (queue[i].x === x && queue[i].y === y) {
      return i
    }
  }
  return -1
}

const parentIndexOf = (index) =>
  Math.floor((index - 1) / 2)

const leftChildIndexOf = (index) =>
  2 * index + 1

const rightChildIndexOf = (index) =>
  2 * index + 2

const isInBounds = (risks, x, y) =>
  x >= 0 && x <= xMax(risks) && y >= 0 && y <= yMax(risks)

const yMax = (risks) =>
  risks[0].length - 1

const xMax = (risks) =>
  risks.length - 1

const isVisited = (visited, x, y) =>
  visited[x]?.[y] ?? false

const setVisited = (visited, x, y) => {
  visited[x] = visited[x] ?? []
  visited[x][y] = true
}

const getCost = (costs, x, y) =>
  costs[x]?.[y] ?? Number.POSITIVE_INFINITY

const setCost = (costs, cost, x, y) => {
  costs[x] = costs[x] ?? []
  costs[x][y] = cost
}

const isDestination = (risks, x, y) =>
  x === xMax(risks) && y === yMax(risks)

export const solvePart1 = () => {
  const risks = readRisks()
  return calcCostOfPath(risks)
}

export const solvePart2 = () => {
  const risks = readRisks()
  const inflatedRisks = inflateRisks(risks, 5)
  return calcCostOfPath(inflatedRisks)
}
