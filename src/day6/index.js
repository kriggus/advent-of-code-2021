import * as fs from 'fs'

const readLanterfishList = () =>
  fs
    .readFileSync('./src/day6/input.txt', 'utf-8')
    .split(',')
    .map(s => parseInt(s))

const stimulateGrowth = (initialFishes, noOfDays) => {
  const maxTimer = 8
  const reproduceRate = 6

  let fishes = new Array(maxTimer + 1).fill(0)
  initialFishes.forEach(initialTimer => {
    fishes[initialTimer]++
  })

  for (let day = 0; day < noOfDays; day++) {
    const nextGeneration = [...fishes]
    for (let timer = 0; timer <= maxTimer; timer++) {
      if (timer === reproduceRate) {
        nextGeneration[timer] = fishes[timer + 1] + fishes[0]
      } else if (timer === maxTimer) {
        nextGeneration[timer] = fishes[0]
      } else {
        nextGeneration[timer] = fishes[timer + 1]
      }
    }
    fishes = nextGeneration
  }

  return fishes.reduce((acc, noOfFish) => acc + noOfFish, 0)
}

export const solvePart1 = () => {
  const fishes = readLanterfishList()
  return stimulateGrowth(fishes, 80)
}

export const solvePart2 = () => {
  const fishes = readLanterfishList()
  return stimulateGrowth(fishes, 256)
}
