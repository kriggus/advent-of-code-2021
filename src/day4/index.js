import * as fs from 'fs'

const readBingoInput = () => {
  const lines = fs
    .readFileSync('./src/day4/input.txt', 'utf-8')
    .split('\n\n')
    .filter(l => l)
  const drawNumbers = lines[0]
    .split(',')
    .map(s => parseInt(s, 10))
  const cards = lines
    .slice(1)
    .map(cs =>
      cs.trim()
        .split('\n')
        .map(ls =>
          ls.split(' ')
            .filter(ns => ns !== '')
            .map(ns => ({
              number: parseInt(ns, 10),
              isMarked: false
            }))
        )
    )
  return { drawNumbers, cards }
}

const playBingo = (setup) => {
  for (const drawNumber of setup.drawNumbers) {
    for (const card of setup.cards) {
      card.forEach(row => {
        row.forEach(cell => {
          cell.isMarked = cell.isMarked || cell.number === drawNumber
        })
      })
      if (isWinner(card)) {
        return { card, drawNumber }
      }
    }
  }
  throw Error('No winner')
}

const playBingoUntilAllWin = (setup) => {
  const remainingsCards = [...setup.cards]
  for (const drawNumber of setup.drawNumbers) {
    for (let i = 0; i < remainingsCards.length; i++) {
      const card = remainingsCards[i]
      card.forEach(row => {
        row.forEach(cell => {
          cell.isMarked = cell.isMarked || cell.number === drawNumber
        })
      })
      if (isWinner(card)) {
        remainingsCards.splice(i, 1)
        if (remainingsCards.length === 0) {
          return { card, drawNumber }
        }
        i--
      }
    }
  }
  throw Error('No winner')
}

const isWinner = (card) =>
  hasCompletedRow(card) || hasCompletedColumn(card)

const hasCompletedRow = (card) =>
  card.some(row => row.every(cell => cell.isMarked))

const hasCompletedColumn = (card) =>
  flipXY(card).some(column => column.every(cell => cell.isMarked))

const flipXY = (card) =>
  card.map((_, i) => [card[0][i], card[1][i], card[2][i], card[3][i], card[4][i]])

const calcScore = (card, drawNumber) => {
  const sumOfUnmarked = card.reduce(
    (sum, row) => sum + row.reduce(
      (rowSum, cell) => rowSum + (cell.isMarked ? 0 : cell.number),
      0
    ),
    0
  )
  return sumOfUnmarked * drawNumber
}

export const solvePart1 = () => {
  const setup = readBingoInput()
  const { card, drawNumber } = playBingo(setup)
  return calcScore(card, drawNumber)
}

export const solvePart2 = () => {
  const setup = readBingoInput()
  const { card, drawNumber } = playBingoUntilAllWin(setup)
  return calcScore(card, drawNumber)
}
