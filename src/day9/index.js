import * as fs from 'fs'

const readHeightMap = () => {
  const map = []
  fs
    .readFileSync('./src/day9/input.txt', 'utf-8')
    .split('\n')
    .filter(l => l)
    .map(row => [...row].map(s => parseInt(s)))
    .forEach((row) =>
      row.forEach((height, x) => {
        map[x] = map[x] ? [...map[x], height] : [height]
      })
    )
  return map
}

const getLowPoints = (map) =>
  map
    .flatMap((column, x) =>
      column.map((height, y) =>
        ({ x, y, height })
      )
    )
    .filter(point =>
      isLowPoint(map, point)
    )

const getBasins = (map) => {
  const lowPoints = getLowPoints(map)
  return lowPoints.map(lowPoint => {
    const points = [lowPoint, ...getAdjecentBasinPoints(map, lowPoint)]
    const set = new Set()
    return points.filter(point => {
      const pointId = `${point.x}${point.y}`
      const shouldKeep = !set.has(pointId)
      set.add(pointId)
      return shouldKeep
    })
  })
}

const getAdjecentBasinPoints = (map, point) =>
  getAdjecentPoints(map, point.x, point.y)
    .filter(adjecentPoint =>
      adjecentPoint.height < 9 && adjecentPoint.height > point.height
    )
    .flatMap(adjecentPoint => [
      adjecentPoint,
      ...getAdjecentBasinPoints(map, adjecentPoint)
    ])

const isLowPoint = (map, point) => {
  const adjecentPoints = getAdjecentPoints(map, point.x, point.y)
  return adjecentPoints.every(adjecent => adjecent.height > point.height)
}

const getAdjecentPoints = (map, xCenter, yCenter) => {
  return [
    { x: xCenter - 1, y: yCenter, height: map[xCenter - 1]?.[yCenter] },
    { x: xCenter + 1, y: yCenter, height: map[xCenter + 1]?.[yCenter] },
    { x: xCenter, y: yCenter - 1, height: map[xCenter]?.[yCenter - 1] },
    { x: xCenter, y: yCenter + 1, height: map[xCenter]?.[yCenter + 1] }
  ].filter(p => isFinite(p.height))
}

const riskLevel = (height) =>
  height + 1

export const solvePart1 = () => {
  const map = readHeightMap()
  const lowPoints = getLowPoints(map)
  return lowPoints.reduce(
    (sum, point) => sum + riskLevel(point.height),
    0
  )
}

export const solvePart2 = () => {
  const map = readHeightMap()
  const basins = getBasins(map)
  return basins
    .sort((b1, b2) => b2.length - b1.length)
    .slice(0, 3)
    .reduce(
      (mul, basin) => mul * basin.length,
      1
    )
}
